/**********************************************
* File: AVLAuthorTest.cpp
* Author: 
* Email: 
*  
**********************************************/
#include <iostream>
#include <string>
#include "AVLTree.h"

struct author
{
	std::string firstName;
	std::string lastName;

	author(std::string firstName, std::string lastName) : firstName(firstName), lastName(lastName) {}

	bool operator<(const author& rhs) const{
		if(lastName < rhs.lastName)
			return true;
		else if( lastName== rhs.lastName)
		{
			if(firstName < rhs.firstName)
				return true;
		}


		return false;
	}

 	bool operator>(const author& rhs) const{
                if(lastName > rhs.lastName)
                        return true;
                else if( lastName== rhs.lastName)
                {
                        if(firstName > rhs.firstName)
                                return true;
                }


                return false;
        }

	bool operator==(const author& rhs) const{
		if(lastName != rhs.lastName)
		{
			return false;
		}
		else
		{
			if (firstName != rhs.firstName)
			{
				return false;
			}
		}

	return true;

	}


	friend std::ostream& operator<<(std::ostream& outStream, const author& printAuth);
	//declare friend and implement later

};

std::ostream& operator<<(std::ostream& outStream, const author& printAuth)
{
	outStream << printAuth.lastName << ", " << printAuth.firstName; 

	return outStream;
}



/********************************************
* Function Name  : main
* Pre-conditions : int argc, char **argv
* Post-conditions: int
*  
* This is the main driver program for the 
* AVLTree function with strings 
********************************************/
int main(int argc, char **argv)
{
	//create AVL tree
	AVLTree<author> authorsAVL;

	//create five authors
    	author one("Anthony","Aardvark");
	author two("Greg","Aardvark");
 	author three("Gayle","McDowell");
 	author four("Michael","Main");
 	author five("BadMean","Person");
	author six("Walter", "Savitch"); 

	authorsAVL.insert(one); 
	authorsAVL.insert(two);
	authorsAVL.insert(three);
	authorsAVL.insert(four);
	authorsAVL.insert(five);
	authorsAVL.insert(six);


	authorsAVL.printTree();


	std::cout<< "Removing " << five << std:: endl; 
	authorsAVL.remove(five);
	authorsAVL.printTree();

    return 0;
}
