//To compile: g++ -g -std=c++11 -Wpedantic AgeHash.cpp -o AgeHash
//Executable: ./AgeHash

//Nicole Benz
//in class DS code 3-27 

/**********************************************
* File: AgeHash.cpp
* Author: Matthew Morrison
* Email: matt.morrison@nd.edu
* 
* Shows the comparison of the input or ordered and
* unordered sets 
**********************************************/
// Libraries Here
#include<map>
#include<unordered_map>
#include<iterator>
#include<iostream>
#include<string>



/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*
* Main driver function. Solution  
********************************************/
int main(int argc, char** argv){
	
	std::map<std::string,int> ageHashOrdered = { {"Matthew",38}, {"Alfred",72}, {"Roscoe",36},{"James",35}}; 

	std::map<std::string,int>::iterator iterOr; 	
	
	std::cout << "The ordered hashes are: " << std::endl; 
	
	for (iterOr = ageHashOrdered.begin(); iterOr != ageHashOrdered.end(); iterOr++)
	{
		std::cout << iterOr->first << " " << iterOr->second << std::endl;
	}
	//puts into AVL tree and then in order traversal prints stuff out in alphabetical order by string
	//sorting when being input when we put the elements in map 
	//point of using avl tree -- delteion marked, not removed --mostly just inserts and finds
	//iterator performs in order traversal of tree 



	std::unordered_map<std::string, int> ageHashUnordered = { {"Matthew",38}, {"Alfred",72}, {"Roscoe",36},{"James",35}};

	//create unordered iterator--different bc of underlying arch
	std::unordered_map<std::string,int>::iterator iterUn; 

	std::cout<< "--------------" << std::endl;
	std::cout << "The Unordered Hashes are: " <<std::endl;

	for (iterUn = ageHashUnordered.begin(); iterUn != ageHashUnordered.end(); iterUn++)
        {
                std::cout << iterUn->first << " " << iterUn->second << std::endl;
        }


	std::cout<< "The ordered example: " << ageHashOrdered["Matthew"] << std::endl;
	std::cout<< "The ordered example: " << ageHashOrdered["James"] << std::endl;


	return 0;
}
