/**********************************************
* File: Gooogle.cpp
* Author: Nicole Benz
* Email: nbenz@nd.edu
* Cadence 4 Google Problem
*
**********************************************/

#include <iostream>
#include <fstream>
#include <unordered_map>
#include <string>
#include <iterator>
#include <cctype>
#include <list>
#include <vector>
/********************************************
* Function Name  : main
* Pre-conditions : int argc, char** argv
* Post-conditions: int
*
* Main driver function for the program  
********************************************/
int main(int argc, char** argv){

	
	//store letters from liscense plate in a map
	//if its a letter, store in hash map in uppercase form
	//loop through all the words in that list and hash each word
	//compare the two hashes of the plate letters and dict word, if there are
	//the same or more of each letter,  add it to matches list
	//once all words have been checked, loop through matches and return the shortest one

	//std::list<std::string> answers; //words that contain the letters, will sort to find shortest
	
	//answer string--will update it every time we fnd a shorter valid word
	std::string answer1 = ""; 
	int len = 999; //random big number everything will be less than


	//for each word
	//std::unordered_map< char, int > dictWord; //string is letter and int is count 

	//for the plate
	 std::unordered_map< char, int > plate; //string is letter and int is count
	

	 
	//loop through plate entered--assume plate entered is the argv input argv[2]
	for (int i = 0; i <= sizeof(argv[2])/sizeof(argv[2][0]); i++)
	{
		//std::cout<<"looping" <<std::endl;
		//only if it is a letter, assume all uppercase
	   if (isalpha(argv[2][i]))
	   {
		// If the word has not yet been hashed
		if(plate.find(argv[2][i]) == plate.end()){
			//not found, so insert that letter
			// Put the word in, and set the count to 1
			plate.insert( {argv[2][i], 1} );
		}
		else{
			// Increment the count 
			plate[argv[2][i]]++;
		}
           }
	}


	/*for(std::unordered_map<char, int>::iterator iter = plate.begin(); iter != plate.end(); iter++){
		std::cout << iter->first << " " << iter->second << std::endl;
	}*/


	//now I have my hash map of lisence plate chars	




	// Get the inputstream
	std::ifstream bookFile;
	bookFile.open(argv[1]);
	if(!bookFile.is_open()){
		std::cout << "The file " << argv[1] << " does not exist. Exiting Program..." << std::endl;
		exit(-1);
	}
	
	// Get the strings and put in list, then we will go thru that list to the Hash Table
	std::string wordIn;
	std::vector<std::string> vec;


	while (bookFile >> wordIn)
	{
		vec.push_back(wordIn); 
	}


	//now I have my vector of dict strings, go through these and hash

	char letterIn; 
	std::string x; 
	for (int v=0; v<vec.size(); v++)
	{
		std::unordered_map< char, int > dictWord; //declares a new hash
		x=vec[v]; //string to be hashed

             for (int s=0; s<x.length(); s++)
	    {
		letterIn = x[s]; 
	
	      if (isalpha(letterIn)) 	
	      {

		// If the word has not yet been hashed
		if(dictWord.find(letterIn) == dictWord.end()){
			//not found, so have to insert
			// Put the word in, and set the count to 1
			dictWord.insert( {letterIn, 1} );
		}
		else{
			// Increment the count 
			dictWord[letterIn]++;
		}
             }

	   }


	//now we have a hash for that word
	//compare to plate hash counts to see if it is valid--if it is, add that string to a list

	//int len = 9999;
	int isValid = 1; 
	for(std::unordered_map<char, int>::iterator iter = plate.begin(); iter != plate.end(); iter++)
	{ 

		//if that letter doesn't exist in that dictionary word, or if the dictionary word doesn't have enough of that letter
     		if (!dictWord.count(iter->first) || dictWord[iter->first] < iter->second)
		{
			//set bool int equal to false and break out of here
			isValid=0; 
			break; 
 
		}           
	}

	//if it turns to 0, then shouldn't add
	if (isValid == 1)
	{
		//std::cout<< x << std::endl;

		if (x.length() < len)
		{
			answer1 = x;
			len = answer1.length();  
		}
	}

     }



	if (answer1.compare("")==0)
	{
		std::cout << "No word found in the dictionary" << std::endl;
		return 1; 
	}

	//std::cout << len << std::endl;	
	std::cout << "Shortest word for that plate in the dictionary is: " << answer1 << std::endl; 



	// Close the ifstream
	bookFile.close();
	

	return 0;

}


