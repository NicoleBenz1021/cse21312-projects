//Nicole Benz

/**********************************************
	
* File: findRoute.cpp
	
* Nicole Benz, netid nbenz
	
* Email: nbenz@nd.edu

This is the program for Weekly Coding Assignment 3, Problem 1:
Given a directed graph, design an algorithm to find whether there 
is a route between two nodes. 
	
 To compile: g++ -g -std=c++11 findRoute.cpp -o findRoute
	
To run: ./findRoute
	
**********************/


#include<iostream>
#include<vector>
#include<string>
#include<list>
#include<iterator>

using namespace std;

struct Edge 
{
	//edge direction is determined by what user enters as third thing
	int source;
	int dest;
	string whichWay; 
	//if whichWay is "both", edge goes from source->dest and dest->source
	//if whichWay is "forward", edge only goes from source->dest
	//if whichWay is "backward", edge only goes from dest->source

};

//create the class for a Directed Graph using adjacency list representation
class Graph
{
	int numVertices; //number of vertices in the graph
	vector<int> *adjVec; //vector containing adjacencey lists

	public:
		Graph(int numVertices); 
		void addEdge(Edge e);
		bool pathExists(int start, int end); 
		//user enters start and end and boolean indicating if path between start and end exists
};

Graph::Graph(int numVertices)
{
	this->numVertices=numVertices; 
	adjVec = new vector<int>(numVertices); 
}

void Graph::addEdge(Edge e)
{	
	cout << "adding an edge" << endl; 

	//this function adds the edge by putting the source/dest in each others lists as appropriate
	
	if (e.whichWay.compare("both")==0)
	{
		adjVec[e.source].push_back(e.dest); //add dest to source's vec
		adjVec[e.dest].push_back(e.source); //add source to dest's vec
	}
	else if (e.whichWay.compare("forward")==0)
	{
		cout << "forward thing" << endl;
		adjVec[e.source].push_back(e.dest); //add dest to source's vec "forward only"
		cout << "after forward thing" << endl; 
	}
	else if (e.whichWay.compare("backward")==0)
	{
		adjVec[e.dest].push_back(e.source); //add source to dest's vec "backward only"
	}
	else
	{
		cout << "Error, unable to add edge to the Graph" << endl; 
	}

}	


//do breadth first search to check whether end is reachable from start
bool Graph:: pathExists(int start, int end)
{
	//first check base case if start is same as end
	if (start==end)
	{
		return true; 
	}

	//mark the vertices as all not visited at the beginning
	//we have to distinguish between visited/not visited to avoid repeats

	cout << "marking vertices" << endl;
	
	bool *visited = new bool[numVertices]; 
	for (int i=0; i<numVertices; i++)
	{
		visited[i] = false; 
	}

	//make a queue for BFS
	list<int> queue; 

	cout << " mark node currrent and add to queue" << endl; 

	//mark the node we visit as visited and then add it to the queue
	visited[start] = true; 
	queue.push_back(start); 

	//iterator to get all the adjacent vertices of a vertex
	//vector<int>::iterator ii; 

	cout << "entering while loop" << endl;
	while (!queue.empty()) //keep going thru queue until we find path or run out
	{
		//dequeue a vertex 
		start = queue.front();
		queue.pop_front(); 
	 
		cout << "entering for loop" << endl;
	// for (ii = adjVec[start].begin(); ii!= adjVec[start].end(); ii++)
 
            for (int ii = 0; ii< adjVec[start].size(); ii++)
	    { 


		//get all the adjacent vertices of start vertex
		//if the adjacent vertex hasn't been visited, then mark it as visited, ad to queue
		if (adjVec[start].at(ii) == end)	
		{
			//we found a path if the adjacent node is the destination node
			return true; 
		}
		else
		{
			//otherwise, keep going
			if (!visited[adjVec[start].at(ii)])
			{
				visited[adjVec[start].at(ii)]=true; //mark as visited
				queue.push_back(adjVec[start].at(ii)); 
			}
		}
            }
	}

	//if we get through all of this without returning true, then path from start to end
	//entered doesn't exist

	return false; 


}

int main()
{
	//create a graph of edges
	
	cout << "creating edges" << endl;


	Edge a;
	a.source = 0; 
	a.dest = 1; 
	a.whichWay= "forward";	


	Edge b;
	b.source = 0; 
	b.dest = 2; 
	b.whichWay= "forward";

	Edge c;
	c.source = 1; 
	c.dest = 2; 
	c.whichWay= "forward";

	Edge d;
	d.source = 2; 
	d.dest = 0; 
	d.whichWay= "forward";

	Edge e;
	e.source = 2; 
	e.dest = 3; 
	e.whichWay= "forward";

	Edge f;
	f.source = 3; 
	f.dest = 3; 
	f.whichWay= "forward";

	/*Edge a(0,1);
	Edge b(0,2);
	Edge c(1,2);
	Edge d(2,0);
	Edge e(2,3);
	Edge f(3,3); */


	cout << "decalre graph" << endl;

	//add edges to graph
	Graph g(6); 
	

	cout << "adding edges to graph" << endl;
	g.addEdge(a);
	cout << "b" << endl;
	g.addEdge(b);
	g.addEdge(c);
	g.addEdge(d);
	g.addEdge(e);
	g.addEdge(f);

	int start = 1;
	int end = 3; 

	cout << "call exists func" << endl;

	if (g.pathExists(start,end))
	{
		cout << "There exists a path from " << start << " to " << end << " for the graph." << endl;
	}	
	else
	{
		cout << "There is no path from " << start << " to " << end << " for the graph." << endl; 
	}

	return 0; 

}


/*

	Source for BFS logic with queue: https://www.geeksforgeeks.org/find-if-there-is-a-path-between-two-vertices-in-a-given-graph/
	Geeks for Geeks: "Graph Find Route Between Vertices" 

*/





