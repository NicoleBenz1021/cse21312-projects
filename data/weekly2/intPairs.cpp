//Nicole Benz
//Weekly Individual Coding Assignemnt--Problem 3
//Design an algorithm to find all pairs of integers within an array which sum to a specified value. 
// netid: nbenz
//3-30-19

#include<map>
#include<unordered_map>
#include<iterator>
#include<iostream>
using namespace std; 

/***************************
* File name: intPairs.cpp
* Author: Nicole Benz
* email: nbenz@nd.edu
* intPairs.cpp contains the function and main driver for Problem 3, 
* finding all pairs of integers within an array which sum to a specified 
* value.
***************************/

/*************************
* Function name: printArray
* Pre-conditions: int arr[], int size
* Post conditions: void
* printArray prints out the array for each test case
* so the user can verify the output is correct
***************************/
void printArray(int arr[], int size, int sum)
{

        cout << "Sum is: " << sum << endl;
	cout << "Array: { ";

	for (int i=0; i<size; i++)
	{
		cout << arr[i] << " ";
	}
	cout << "}" << endl; 

}



/***************************
* Function name: findPairs
* Pre-conditions: int arr[], int sum
* Post conditions: int count
* findPairs takes in an array of integers and uses a map/hashing
* to find all the pairs of integers within an array which sum
* to a specific value (sum). Also prints out the pair when it finds one
***************************/

int findPairs(int arr[], int sum, int arrSize)
{
	int diff; //difference between current array element and the sum
	
	int count = 0; //number of pairs
	int c; 	


	//int arrSize = sizeof(arr)/sizeof(arr[0]); //get number of elements in array

	//use unordered map to store counts of all the elements
	unordered_map<int,int> intCounts; //key is the int in the array
	//the count/value is the number is the number of times the int is found in the array


	for (int i=0; i < arrSize; i++)
	{
		//figure out if a pair can be found for the current index spot of array
		diff = sum - arr[i];

		//for a pair to form the sum with arr[i], we have to look for diff
		//that's what would give us a pair


		//use map find() method -- returns .end() if not found, or iterator to element if found
		if (intCounts.find(diff) != intCounts.end() )
		{
			c = intCounts[diff]; 
			for ( int ii = 0; ii<c; ii++)
			{
				count++; //increment number of pairs found
				cout << "(" << diff << ", " << arr[i] << ")" << endl; 
			}		
		}
		
		intCounts[arr[i]]++; //increment number of times arr[i]  was found

	}

	return(count); 

}



/***************************
* Function name: printResults
* Pre-conditions: int numPaits
* Post conditions: void
* This function prints out a statement indicating how many pairs were found in a given array.
***************************/
void printResults(int numPairs)
{

  //print statement of how many pairs found
        if (numPairs == 1)
        {
                cout << "There was " << numPairs << " pair found in this array." << endl;
        }
	else
	{
                cout << "There were " << numPairs << " pairs found in this array." << endl;
        }


}

void printSep()
{
	cout << " " << endl; 
	cout << "_________________________________" << endl;
	cout << " " << endl; 
}




/***************************
* Function name: main
* Pre-conditions: none
* This is the main driver for the findPairs function which 
* tests several test cases to ensure the fucntionality of the program. 
***************************/

int main()
{
	//test 1
	int arr1[] = {1,5,7,-1,5}; 
	int sum1 = 6; 
	//int size = 5; 
	//call function
	printArray(arr1,5,6);
	int numPairs1 = findPairs(arr1, sum1, sizeof(arr1)/sizeof(arr1[0]));
	printResults(numPairs1); 

	printSep(); 

	//test 2 -- no pairs
	int arr2[] = {3,6,7,8,11,98,3,1,-10};
        int sum2 = 100;
        //call function
        printArray(arr2,9,100);
	int numPairs2 = findPairs(arr2, sum2, sizeof(arr2)/sizeof(arr2[0]));
        printResults(numPairs2);

	printSep();

	//test 3
        int arr3[] = {4,5,4,4,4,4,4};
        int sum3 = 8;
        //call function
	printArray(arr3,7,8);
        int numPairs3 = findPairs(arr3, sum3, sizeof(arr3)/sizeof(arr3[0]));
        printResults(numPairs3);

	printSep(); 

	//test 4
        int arr4[] = {3,-3,1,-1,2,-2,4,6,8,9,10};
        int sum4 = 5;
        //call function
	printArray(arr4,11,5);
        int numPairs4 = findPairs(arr4, sum4, sizeof(arr4)/sizeof(arr4[0]));
        printResults(numPairs4);

	printSep();

	//test 5
        int arr5[] = {1,1,0,2,3,4,0,3,1};
        int sum5 = 4;
        //call function
	printArray(arr5,9,4);
        int numPairs5 = findPairs(arr5, sum5, sizeof(arr5)/sizeof(arr5[0]));
        printResults(numPairs5);

	printSep();

        //test 6
        int arr6[] = {1,2,3,4};
        int sum6 = 3;
        //call function
	printArray(arr6,4,3);
        int numPairs6 = findPairs(arr6, sum6, sizeof(arr6)/sizeof(arr6[0]));
        printResults(numPairs6);


	/*printSep();
	cout << " " << endl; 
	cout << "Dr. Morrison's Test Cases: Array {1,0,4,6,3,2,6,7,7,0,6}" << endl;
	cout << " " << endl;

	cout << "Sum = 7" << endl; */



}

